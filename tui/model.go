package tui

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/wiggins.jonathan/harmony/flac"

	"github.com/charmbracelet/bubbles/filepicker"
	tea "github.com/charmbracelet/bubbletea"
)

type Model struct {
	filepicker   filepicker.Model
	selectedFile string
	quitting     bool
	err          error
}

type clearErrorMsg struct{}

func clearErrorAfter(t time.Duration) tea.Cmd {
	return tea.Tick(t, func(_ time.Time) tea.Msg {
		return clearErrorMsg{}
	})
}

// NewModel is a constructor function that sets up the TUI with default options
func NewModel() (*Model, error) {
	fp := filepicker.New() // get default filepicker

	currentDirectory, err := os.Getwd() // set currentDirectory
	if err != nil {
		return nil, fmt.Errorf("Can't get current working directory: %w\n", err)
	}
	fp.CurrentDirectory = currentDirectory

	// modify defaults
	fp.AllowedTypes = []string{".flac"}
	fp.ShowPermissions = false
	fp.ShowSize = false

	return &Model{filepicker: fp}, nil
}

func (m Model) Init() tea.Cmd {
	return m.filepicker.Init()
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			m.quitting = true
			return m, tea.Quit
		}
	case clearErrorMsg:
		m.err = nil
	}

	var cmd tea.Cmd
	m.filepicker, cmd = m.filepicker.Update(msg)

	// Did the user select a file?
	if didSelect, path := m.filepicker.DidSelectFile(msg); didSelect {
		ok, err := flac.Validate(path)
		if err != nil {
			m.err = errors.New("Could not open " + path)
			m.selectedFile = ""
		}
		if !ok {
			m.err = errors.New(path + " is not a valid .flac file")
			m.selectedFile = ""
			return m, tea.Batch(cmd, clearErrorAfter(1*time.Second))
		}

		m.selectedFile = path
	}

	// Did the user select a file not in allowedTypes?
	if didSelect, path := m.filepicker.DidSelectDisabledFile(msg); didSelect {
		// then clear the selectedFile and display an error.
		m.err = errors.New(path + " is not supported.")
		m.selectedFile = ""
		return m, tea.Batch(cmd, clearErrorAfter(1*time.Second))
	}

	return m, cmd
}

func (m Model) View() string {
	if m.quitting {
		return ""
	}

	var s strings.Builder
	s.WriteString("\n  ")
	if m.err != nil {
		s.WriteString(m.filepicker.Styles.DisabledFile.Render(m.err.Error()))
	} else if m.selectedFile == "" {
		s.WriteString("Pick a file:")
	} else {
		s.WriteString("Selected file: " + m.filepicker.Styles.Selected.Render(m.selectedFile))
	}
	s.WriteString("\n\n" + m.filepicker.View() + "\n")
	return s.String()
}
