package main

import (
	"fmt"
	"os"

	"gitlab.com/wiggins.jonathan/harmony/tui"

	tea "github.com/charmbracelet/bubbletea"
)

func main() {
	m, err := tui.NewModel()
	if err != nil {
		fmt.Printf("Error creating model: %s\n", err)
		os.Exit(1)
	}

	_, err = tea.NewProgram(m, tea.WithOutput(os.Stderr)).Run()
	if err != nil {
		fmt.Printf("Error starting TUI: %s\n", err)
		os.Exit(1)
	}
}
