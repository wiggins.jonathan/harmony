package flac

import (
	"fmt"
	"io"
	"os"
)

// Validate takes in an io.Reader & reads first 4 bits to determine if
// this is, in fact, a valid flac file
func Validate(file string) (bool, error) {
	f, err := os.Open(file)
	if err != nil {
		return false, fmt.Errorf("Failed to open %s: %w", file, err)
	}
	defer f.Close()

	header := make([]byte, 4)
	_, err = io.ReadFull(f, header[:])
	if err != nil {
		return false, fmt.Errorf("Validate couldn't read header: %w", err)
	}

	return string(header) == "fLaC", nil
}
